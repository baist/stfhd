project(stfhd)
cmake_minimum_required(VERSION 2.8)
#set(Boost_USE_STATIC_LIBS OFF)
#set(Boost_USE_MULTITHREADED ON)
#set(Boost_USE_STATIC_RUNTIME OFF)
#find_package(Boost REQUIRED)
#include_directories(${Boost_INCLUDE_DIRS})
#include_directories(./include)
set(SRC_LIST
  ./Element.cpp
  ./Parser.cpp
  ./Variant.cpp
)
add_library(${PROJECT_NAME} SHARED ${SRC_LIST})
target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES})
