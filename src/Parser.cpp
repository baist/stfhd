#include <stack>
#include <stdexcept>
#include "Parser.h"

namespace stfhd
{

Parser::Parser(std::iostream* stream):
  m_iostream(stream)
{
}

Parser::~Parser()
{
  for(ElementsList::const_iterator it = m_allElements.begin(),
      end = m_allElements.end(); it != end; ++it) {
    delete (*it);
  }
}

void Parser::parse()
{
  std::stack<Element*> stackElements;
  stackElements.push(0);
  std::string accumulator;
  bool lastIsBackslash = false;
  while(!m_iostream->eof()) {
    char chr = m_iostream->get();
    if(!m_iostream->good()) {
      break;
    }
    Element* parentElement = stackElements.top();
    const int accLen =  accumulator.length();
    const bool accIsNotEmpty = accLen > 0;
    const bool lastIsSpace = accIsNotEmpty ?
          std::isspace(accumulator[accLen - 1]) : false;
    const bool hasParent = parentElement != 0;
    if(lastIsBackslash) {
      if((chr == '\\') || (chr == '}') || (chr == '}')) {
        accumulator.push_back(chr);
      }
      else if(chr == 's') {
        accumulator.push_back(' ');
      }
      else if(chr == 't') {
        accumulator.push_back('\t');
      }
      else if(chr == 'r') {
        accumulator.push_back('\r');
      }
      else if(chr == 'n') {
        accumulator.push_back('\n');
      }
      else if(chr == 'v') {
        accumulator.push_back('\v');
      }
      else {
        std::string str("\\");
        str += chr;
        str += " is not escape symbol";
        throw std::runtime_error(str);
      }
      lastIsBackslash = false;
    }
    else if(chr == '\\') {
      lastIsBackslash = true;
    }
    else if(std::isspace(chr)) {
      if(accIsNotEmpty && !lastIsSpace) {
        accumulator.push_back(chr);
      }
    }
    else if(chr == '{') {
      if(accIsNotEmpty) {
        Element* element = new Element(
              accumulator.substr(0, lastIsSpace ? accLen - 1 : accLen),
              parentElement);
        m_allElements.push_back(element);
        if(!hasParent) {
          m_childElements.push_back(element);
        }
        stackElements.push(element);
        accumulator.clear();
      }
      else {
        throw std::runtime_error("starting element without name");
      }
    }
    else if(chr == '}') {
      if(hasParent) {
        if(accIsNotEmpty) {
          parentElement->addValue(
                Variant(accumulator.substr(0, lastIsSpace? accLen - 1 : accLen)));
        }
        stackElements.pop();
        accumulator.clear();
      }
      else {
        throw std::runtime_error("closing non-existent element");
      }
    }
    else {
      if(lastIsSpace) {
        if(hasParent) {
          parentElement->addValue(Variant(accumulator.substr(0, accLen - 1)));
          accumulator.clear();
          accumulator.push_back(chr);
        }
        else {
          throw std::runtime_error("there is a value in zero element?");
        }
      }
      else {
        accumulator.push_back(chr);
      }
    }
  }
  if(stackElements.size() > 1) {
    throw std::runtime_error("unexpected end of stream");
  }
}

int Parser::childCount() const
{
  return m_childElements.size();
}

const Element* Parser::child(int index) const
{
  if(index > m_childElements.size()) {
    throw std::out_of_range ("list of elements, out of range");
  }
  ElementsList::const_iterator it = m_childElements.begin();
  std::advance(it, index);
  return *it;
}

}
