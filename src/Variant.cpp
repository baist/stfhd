#include <boost/lexical_cast.hpp>
#include "Variant.h"

namespace stfhd
{
  
Variant::Variant(bool value)
{
  if(value) {
    m_value = "1";
  }
  else {
    m_value = "0";
  }
}

Variant::Variant(char value)
{
  m_value = boost::lexical_cast<std::string>(value);
}

Variant::Variant(int value)
{
  m_value = boost::lexical_cast<std::string>(value);
}

Variant::Variant(float value)
{
  m_value = boost::lexical_cast<std::string>(value);
}

Variant::Variant(double value)
{
  m_value = boost::lexical_cast<std::string>(value);
}

Variant::Variant(const char* value):
  m_value(value)
{
}

Variant::Variant(std::string value):
  m_value(value)
{
}

Variant::~Variant()
{
}

bool Variant::isBool() const
{
  if(!m_value.compare("1") || !m_value.compare("0")) {
    return true;
  }
  return false;
}

bool Variant::isInt() const
{
  for(std::string::const_iterator it = m_value.begin(); 
    it != m_value.end(); it++) {
    if(!isdigit(*it)) {
      return false;
    }
  }
  return true;
}

bool Variant::isFloat() const
{
  for(std::string::const_iterator 
    it = m_value.begin(); it != m_value.end(); it++) {
    if(!isdigit(*it) && (*it != '.')) {
      return false;
    }
  }
  return true;
}

bool Variant::toBool() const
{
  if(!m_value.compare("1")) {
    return true;
  }
  return false;
}

int Variant::toInt() const
{
  return boost::lexical_cast<int>(m_value.c_str());
}

float Variant::toFloat() const
{
  return boost::lexical_cast<double>(m_value.c_str());
}

const std::string& Variant::toStdString() const
{
  return m_value;
}

}
