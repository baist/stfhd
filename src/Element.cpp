#include <stdexcept>
#include "Element.h"

namespace stfhd
{

Element::Element(std::string name, Element* parent):
  m_name(name),
  m_parentElement(parent)
{
  if(m_parentElement) {
    m_parentElement->m_childElements.push_back(this);
  }
}


Element::~Element()
{
}

std::string Element::name() const
{
  return m_name;
}

void Element::addValue(const Variant& value)
{
  m_values.push_back(value);
}

int Element::childCount() const
{
  return m_childElements.size();
}

const Element* Element::child(int index) const
{
  ElementsList::const_iterator it = m_childElements.begin();
  std::advance(it, index);
  return *it;
}

int Element::valueCount() const
{
  return m_values.size();
}

const Variant& Element::value(int index) const
{
  if(index > m_values.size()) {
    throw std::out_of_range("list of values, out of range");
  }
  VariantsList::const_iterator it = m_values.begin();
  std::advance(it, index);
  return *it;
}

}
