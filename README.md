stfhd
=====

Parser for stfhd (Simple text format of hierarchical data).
Compatible only ASCII text(0x00 - 0x7F code symbols). Also escape symbols:
\s, \t, \r, \n, \v, \\, \{, \}.

stfhd is analog of JSON, but simpler.

Example:
***
Project
{
  Name {MyProject}
  GNUgpp
  { 
  }
  GNUld
  {
    Libraries {
      kernel32
      user32
      lgdi32 winspool
      shell32 ole32 
      oleaut32 
      uuid 
      comdlg32 
      advapi32
      stdc++ 
      mingw32 
      gcc 
      gcc_s 
      moldname 
      mingwex
      msvcrt 
      kernel32 
      mingw32 
      iconv
    }
    CRTTypeLinking {static}
  }
  Files {
    programm0.cpp
    programm1.cpp
    programm2.cpp
  }
}
***

where space ' ' using for splitting separate values in list
where '{' '}' are begin and end of blocks that defines child elements. And 
string is name of block(node).
string without '{' '}' is value.