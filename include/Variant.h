#if !defined(VARIANT_H)
#define VARIANT_H
#include <vector>
#include <string>

namespace stfhd
{

class Variant
{
public:
  Variant(bool value);
  Variant(char value);
  Variant(int value);
  Variant(float value);
  Variant(double value);
  Variant(const char* value);
  Variant(std::string value);
  virtual ~Variant();
  bool isBool() const;
  bool isInt() const;
  bool isFloat() const;
  bool toBool() const;
  int toInt() const;
  float toFloat() const;
  const std::string& toStdString() const;
private:
  std::string m_value;
};

typedef std::vector<Variant> VariantsList;

}

#endif // VARIANT_H
