#if !defined(PARSER_H)
#define PARSER_H
#include <iostream>
#include "Element.h"

namespace stfhd
{

class Parser
{
public:
  explicit Parser(std::iostream* stream);
  virtual ~Parser();
  void parse();
  int childCount() const;
  const Element* child(int index) const;
private:
  std::iostream* m_iostream;
  ElementsList m_allElements;
  ElementsList m_childElements;
};

}

#endif // PARSER_H
