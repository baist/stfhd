#if !defined(ELEMENT_H)
#define ELEMENT_H
#include <string>
#include <vector>
#include "Variant.h"

namespace stfhd
{

class Element;
typedef std::vector<Element*> ElementsList;

class Element
{
public:
  explicit Element(std::string name, Element* parent);
  virtual ~Element();
  std::string name() const;
  void addValue(const Variant& value);
  int childCount() const;
  const Element* child(int index) const;
  int valueCount() const;
  const Variant& value(int index) const;
private:
  std::string m_name;
  VariantsList m_values;
  Element* m_parentElement;
  ElementsList m_childElements;
};

}

#endif // ELEMENT_H
